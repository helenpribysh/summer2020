﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HopPlayer : MonoBehaviour
{
    [SerializeField] private AnimationCurve m_JumpCurve;
    [SerializeField] private float m_JumpHeight = 1f;
    [SerializeField] private float m_JumpDistanse = 2f;
    
    [SerializeField] private float m_Ballspeed = 1f;
    [SerializeField] private HopInput m_Input;
    [SerializeField] private HopTrack m_Track;

    private float iteration; //цикл прыжка
    private float startZ; //точка начала прыжка
    
   // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        //смещение
        pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 15f);
        
        //прыжок
        pos.y = m_JumpCurve.Evaluate(iteration) * m_JumpHeight;
        
        //движение вперед
        pos.z = startZ + iteration * m_JumpDistanse;

        transform.position = pos; 
        
        //увеличиваем счетчик прыжка
        iteration += Time.deltaTime * m_Ballspeed;

        if (iteration < 1f)
        {
            return;
        }

        iteration = 0f;
        startZ += m_JumpDistanse;

        if (m_Track.IsBallOnPlatform(transform.position))
        {
            return;
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
