﻿using System.Collections;
using System.Collections.Generic;
using ColorSnake;
using UnityEngine;

public class ColorSnake_LevelGenerator : MonoBehaviour
{
    [SerializeField] private ColorSnake_Types m_Types;
    [SerializeField] private ColorSnake_GameController m_Controller;

    private int line = 1;

    private List<GameObject> obstacles = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {
        var upBoarder = m_Controller.Bounds.Up;

        while (line * 2 < upBoarder + 2f)
        {
            GenerateObstacle();
        }
    }

    // Update is called once per frame
    void Update()
    {
        var upBorder = m_Controller.Bounds.Up + m_Controller.Camera.transform.position.y;
        if (line * 2 > upBorder + 2)
        {
            return;
        }
        
        GenerateObstacle();
        
        // TODO УНИЧТОЖЕНИЕ НИЖНИХ объектов написать самостоятельно
    }

    private void GenerateObstacle()
    {
        var template = m_Types.GetRandomTemplate();
        var  obstacle = new GameObject($"Obstacle_{line}");

        foreach (var point in template.Points)
        {
            var objType = m_Types.GetRandomObjectType();
            var colorType = m_Types.GetRandomColorType();

            var obj = Instantiate(objType.Object, point.position, point.rotation);
            obj.transform.parent = obstacle.transform;

            obj.GetComponent<SpriteRenderer>().color = colorType.Color;

            var obstacleComponent = obj.AddComponent<ColorSnake_Obstacle>();
            obstacleComponent.ColorId = colorType.Id;
        }

        Vector3 pos = obstacle.transform.position;
        pos.y = line * 2;

        obstacle.transform.position = pos;

        line++;
        
        obstacles.Add(obstacle);
    }
}
